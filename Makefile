cc = gcc
defflags = -O2 -lpthread
debugflags = -g -lpthread

default:
	$(cc) $(defflags) mergesort.c -o mergesort

debug:
	$(cc) $(debugflags) mergesort.c -o mergesort
