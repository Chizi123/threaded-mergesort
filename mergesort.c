#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include <limits.h>

#define TEST 1
#define MAX_TEST 1073741825
#define CACHE_SIZE 32
#define THREAD_SIZE 1024

struct mergesort_input {
	int *A;
	unsigned int n;
	int *left;
};

void * threaded_mixsort(void *in);
void insertion_sort(int *A, int n);
void *threaded_mergesort(void *in);
void *nonthreaded_mergesort(void *in);
int general_test(int argc, char **argv);
void timing_test(void);


int main(int argc, char **argv)
{
	switch (TEST) {
	case 1: timing_test();
		break;
	default:
		return general_test(argc, argv);
	}
}

void *threaded_mixsort(void *in)
{
	struct mergesort_input input = *((struct mergesort_input *) in);
	int *right = input.A + input.n/2;
	unsigned int lnum = 0, rnum = 0, pos;
	struct mergesort_input rin, lin;
	lin.A=input.A; lin.n=input.n/2; lin.left=input.left;
	rin.A=input.A+input.n/2; rin.n=input.n-input.n/2; rin.left=input.left+input.n/2;
	pthread_t rthread;
	if (input.n < CACHE_SIZE) {
		insertion_sort(input.A, input.n);
		return NULL;
	}
	else if (input.n < THREAD_SIZE) {
		threaded_mergesort((void*) &rin);
		threaded_mergesort((void *) &lin);

	}
	else {
		pthread_create(&rthread, NULL, threaded_mergesort, (void *) &rin);
		threaded_mergesort((void *) &lin);
		pthread_join(rthread, NULL);

	}
	for (unsigned int i = 0; i < input.n/2+1; i++)
		input.left[i] = input.A[i];
	for (pos = 0; pos < input.n; pos++) {
		if (input.left[lnum] < right[rnum]) {
			input.A[pos] = input.left[lnum];
			lnum++;
		} else {
			input.A[pos] = right[rnum];
			rnum++;
		}
		if (lnum >= input.n/2 || rnum >= input.n-input.n/2) {
			break;
		}
	}
	for (unsigned int i = lnum; i < input.n/2; i++)
		input.A[++pos] = input.left[i];
	return NULL;
}

void insertion_sort(int *A, int n)
{
	for (int i = 0; i < n; i++) {
		int x = A[i];
		int j;
		for (j = i-1; j > 0; j--) {
			if (A[j] > x)
				A[j+1] = A[j];
			A[j+1]=x;
		}
	}
}


void *threaded_mergesort(void *in)
{
	struct mergesort_input input = *((struct mergesort_input *) in);
	int *right = input.A + input.n/2;
	unsigned int lnum = 0, rnum = 0, pos;
	struct mergesort_input rin, lin;
	lin.A=input.A; lin.n=input.n/2; lin.left=input.left;
	rin.A=input.A+input.n/2; rin.n=input.n-input.n/2; rin.left=input.left+input.n/2;
	pthread_t rthread;
	if (input.n > 2) {
		threaded_mergesort((void *) &lin);
		pthread_create(&rthread, NULL, threaded_mergesort, (void *) &rin);
		pthread_join(rthread, NULL);
	}
	for (unsigned int i = 0; i < input.n/2+1; i++)
		input.left[i] = input.A[i];
	for (pos = 0; pos < input.n; pos++) {
		if (input.left[lnum] < right[rnum]) {
			input.A[pos] = input.left[lnum];
			lnum++;
		} else {
			input.A[pos] = right[rnum];
			rnum++;
		}
		if (lnum >= input.n/2 || rnum >= input.n-input.n/2) {
			break;
		}
	}
	for (unsigned int i = lnum; i < input.n/2; i++)
		input.A[++pos] = input.left[i];
	return NULL;
}

void *nonthreaded_mergesort(void *in)
{
	struct mergesort_input input = *((struct mergesort_input *) in);
	int *right = input.A + input.n/2;
	unsigned int lnum = 0, rnum = 0, pos;
	struct mergesort_input rin, lin;
	lin.A=input.A; lin.n=input.n/2; lin.left=input.left;
	rin.A=input.A+input.n/2; rin.n=input.n-input.n/2; rin.left=input.left+input.n/2;
	if (input.n > 2) {
		nonthreaded_mergesort((void *) &lin);
		nonthreaded_mergesort((void *) &rin);
	}
	for (unsigned int i = 0; i < input.n/2+1; i++)
		input.left[i] = input.A[i];
	for (pos = 0; pos < input.n; pos++) {
		if (input.left[lnum] < right[rnum]) {
			input.A[pos] = input.left[lnum];
			lnum++;
		} else {
			input.A[pos] = right[rnum];
			rnum++;
		}
		if (lnum >= input.n/2 || rnum >= input.n-input.n/2) {
			break;
		}
	}
	for (unsigned int i = lnum; i < input.n/2; i++)
		input.A[++pos] = input.left[i];
	return NULL;
}

int general_test(int argc, char **argv)
{
	srand(time(NULL));
	printf("%d\n",argc);
	if (argc < 2 || argc > 3) {
		printf("Invalid argument, use option -h for help\n");
		return 1;
	}
	if (argv[1][0] == '-') {
		if (argv[1][1] == 'h') {
			printf("Mergesort implimentation, written by Joel Grunbaum\n\n");
			printf("Invocation should be of the form \"mergesort [threaded?] [number of sort objects]\"\n");
			printf("Where if \"threaded?\" is 0 it is non mutlithreaded and is threaded for non 0 values\n");
			printf("The second argument determines the number of items to be sorted\n");
			return 2;
		}
		return 1;
	}
	unsigned int num_elems = atoi(argv[2]);
	int *A = malloc(sizeof(int) * num_elems);
	for (unsigned int i = 0; i < num_elems; i++)
		A[i] = num_elems-i;
	for (unsigned int i = 0; i < num_elems; i++)
		printf("%d ", A[i]);
	printf("\n");
	int *left = malloc(sizeof(int) * num_elems);
	struct mergesort_input iin;
	iin.A=A; iin.n=num_elems; iin.left=left;
	if (atoi(argv[1]) == 0)
		nonthreaded_mergesort((void *) &iin);
	else
		threaded_mixsort((void *) &iin);
	for (unsigned int i = 0; i < num_elems; i++)
		printf("%d ", A[i]);
	printf("\n");
	free(A);
	free(left);
	return 0;
}

void timing_test(void)
{
	srand(time(NULL));
	FILE *out = fopen("timimgs.csv", "w");
	fprintf(out,"number of items,mix speed,nonthreaded speed\n");
	int *list = malloc(sizeof(int) * MAX_TEST);
	for (unsigned int i = 0; i < MAX_TEST; i++) {
		list[i] = rand();
	}
	struct mergesort_input in;
	in.A = malloc(sizeof(int) * MAX_TEST); in.left = malloc(sizeof(int) * MAX_TEST);
	time_t now = time(NULL);
	for (unsigned int i = 2; i <= MAX_TEST; i*=2) {
		fprintf(out, "%u,",i);
		if (now != time(NULL)) {
			printf("\r%u",i);
			fflush(stdout);
			fflush(out);
			now = time(NULL);
		}
		struct timespec s, e, net;
		in.n = i;
		for (unsigned int j = 0; j < i; j++) {
			in.A[j] = list[j];
		}
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &s);
		threaded_mergesort((void *) &in);
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &e);
		net.tv_sec = e.tv_sec-s.tv_sec;
		net.tv_nsec = e.tv_nsec-s.tv_nsec;
		if (net.tv_nsec < 0)
			net.tv_nsec = 1000000000+net.tv_nsec;
		fprintf(out, "%lu.%09li,", net.tv_sec, net.tv_nsec);
		for (unsigned int j = 0; j < i; j++) {
			in.A[j] = list[j];
		}
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &s);
		nonthreaded_mergesort((void *) &in);
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &e);
		net.tv_sec = e.tv_sec-s.tv_sec;
		net.tv_nsec = e.tv_nsec-s.tv_nsec;
		if (net.tv_nsec < 0)
			net.tv_nsec = 1000000000+net.tv_nsec;
		fprintf(out, "%lu.%09li\n", net.tv_sec, net.tv_nsec);
	}
	free(in.A); free(in.left); free(list);
	fclose(out);
	printf("\n");
}

